#include "U8glib.h"
#include <Encoder.h>

U8GLIB_NHD31OLED_GR u8g(10, 9);  // SPI Com: SCK = 13, MOSI = 11, CS = 10, A0 = 9

Encoder encoder(2,3);
int enc=0;


// 64x55
const uint8_t logo[] U8G_PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x01, 0xc0, 0x00, 0x00, 0x01, 0xc0, 0x00, 0x00, 0x01, 
  0xe0, 0x00, 0x00, 0x03, 0xc0, 0x00, 0x00, 0x01, 0xf8, 0x00, 0x00, 0x07, 0xc0, 0x00, 0x00, 0x00, 0xf8, 0x00, 0x00, 0x0f, 0xc0, 
  0x00, 0x00, 0x00, 0xf0, 0x00, 0x00, 0x03, 0xc0, 0x00, 0x00, 0x00, 0xf0, 0x00, 0x00, 0x06, 0xc0, 0x00, 0x00, 0x00, 0x98, 0x00, 
  0x00, 0x06, 0x80, 0x00, 0x01, 0xc0, 0x18, 0xff, 0xff, 0xcc, 0x01, 0xe0, 0x01, 0xfc, 0x0f, 0xff, 0xff, 0xfc, 0x1f, 0xe0, 0x00, 
  0xfc, 0x0e, 0x00, 0x00, 0x3c, 0x1f, 0xc0, 0x00, 0x7c, 0x18, 0x00, 0x00, 0x0e, 0x0f, 0x80, 0x00, 0x7e, 0x30, 0x00, 0x00, 0x07, 
  0x3b, 0x00, 0x00, 0x33, 0xe0, 0x00, 0x00, 0x03, 0xf6, 0x00, 0x00, 0x01, 0xc2, 0x00, 0x00, 0x21, 0xc0, 0x00, 0x00, 0x00, 0xc7, 
  0x00, 0x00, 0x70, 0xc0, 0x00, 0x00, 0x00, 0x87, 0x00, 0x00, 0x70, 0xc0, 0x00, 0x00, 0x01, 0x82, 0x00, 0x00, 0x20, 0xc0, 0x00, 
  0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x01, 0x80, 0x0f, 0xfc, 0x00, 0x60, 0x00, 0x00, 0x01, 0x80, 0x0f, 0xfc, 
  0x00, 0x60, 0x00, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x40, 0x00, 0x00, 0x01, 
  0x80, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x01, 0x80, 
  0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x01, 0x80, 0x00, 0x00, 0x00, 0x70, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x0f, 0xf8, 0x00, 
  0x00, 0x07, 0xfc, 0x00, 0x00, 0x1f, 0xfc, 0x00, 0x00, 0x1f, 0xfe, 0x00, 0x00, 0x70, 0x07, 0xff, 0xff, 0xf8, 0x07, 0x80, 0x01, 
  0xe0, 0x01, 0xff, 0xff, 0xe0, 0x03, 0xe0, 0x07, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x01, 0xf8, 0x05, 0xe0, 0x00, 0x00, 0x00, 0x00, 
  0x01, 0xf8, 0x03, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x01, 0xe0, 0x00, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x80, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0xe0, 0x00, 0x00, 0xe0, 0x01, 0x81, 0x00, 0x01, 0xf0, 0x00, 0x00, 0x60, 
  0x01, 0x80, 0x00, 0x03, 0xb8, 0x04, 0x78, 0x61, 0xe3, 0xe7, 0x00, 0x03, 0x19, 0x8c, 0xfc, 0x63, 0xf3, 0xe7, 0x00, 0x03, 0x18, 
  0xdd, 0xce, 0x63, 0x19, 0x83, 0x00, 0x03, 0xf8, 0x79, 0x86, 0x62, 0x19, 0x83, 0x00, 0x03, 0xf8, 0x71, 0x86, 0x62, 0x19, 0x83, 
  0x00, 0x03, 0x18, 0xf9, 0x86, 0x63, 0x19, 0x83, 0x00, 0x03, 0x19, 0xcc, 0xfc, 0xf3, 0xf0, 0xe7, 0xc0, 0x02, 0x19, 0x84, 0x78, 
  0xf1, 0xf0, 0x67, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f
};


uint8_t menu_redraw_required = 0;

const uint8_t uiKeySelect = 4;
uint8_t last_key_state = HIGH;
long key_pressed_tm = 0;

bool edit_value=false;
bool can_edit_value=false;

#define IDLE_TIMEOUT 10000
long ui_touched_tm=0;
bool idle_mode=false;


static const int UI_CLOCK = 7;
static const int KEY_LOAD = 5;
static const int KEY_DATA = 6;

static const int NUM_KEYS = 15;

// debouncer
static const int INTEGRATE_SHIFT = 1;
static const int INTEGRATE_MAX = 1<<INTEGRATE_SHIFT;

static const int KEY_PULSE_WIDTH_USEC = 1;
static const int KEY_POLL_DELAY_MSEC  = 0;

uint16_t ui_keys = 0;
int8_t ui_keys_dbstate[(NUM_KEYS/8+1)*8] = {0};

int key_state = ~0;
int key_idx[13] = {
  0,  // d
  12, // d#
  1,  // e
  2,  // f
  11, // f#
  3,  // g
  10, // g#
  4,  // a
  9,  // a#
  5,  // b
  6,  // c
  8,  // c#
  7,  // d
};
const char *key_names[13] = {"d", "d#", "e", "f", "f#", "g", "g#", "a", "a#", "b", "c", "c#", "d"};

int octave = 0;
uint8_t channel = 0;


void read_ui(void) {
  const long tm=millis();
  bool ui_touched=false;
  long e = encoder.read();
  if(e>=enc+2){
    Serial.write(edit_value?'l':'u');
    ui_touched=true;
    enc=e;
  }
  else if(e<=enc-2){
    Serial.write(edit_value?'r':'d');
    ui_touched=true;
    enc=e;
  }

  const int ks=digitalRead(uiKeySelect);
  if(ks==LOW && last_key_state!=LOW){
    // register key press
    key_pressed_tm=tm;
    ui_touched=true;
  }
  else if(ks==HIGH && last_key_state!=HIGH){
    // register key release
    const long kt=tm-key_pressed_tm;
    if(kt>500){
      if(edit_value){
        edit_value=false;
      }
      else{
        Serial.write('b');
      }
    }
    else{
      if(can_edit_value){
        edit_value=!edit_value;
      }
      else{
        Serial.write('e');
      }
    }
    ui_touched=true;
  }
  last_key_state=ks;
  if(ui_touched){
    ui_touched_tm=tm;
    idle_mode=false;
  }
}


#define MENU_ITEMS 7
char menu_strings[MENU_ITEMS+1][40];

int w=0;
int h=0;

void setupMenu(void){
  int i;
  for(i=0;i<MENU_ITEMS;i++){
    strcpy(menu_strings[i], "                                      ");
  }
    
  u8g.setFont(u8g_font_lucasfont_alternate);
  u8g.setFontRefHeightText();
  u8g.setFontPosTop();
  
  h = u8g.getFontAscent()-u8g.getFontDescent();
  w = u8g.getWidth();
}


bool disp_init=false;

void draw(void){
  uint8_t i;

  if(idle_mode){
    return;
  }

  if(!disp_init){
    u8g.setDefaultForegroundColor();
    u8g.drawBitmapP(96, 5, 8, 55, logo);
    return;
  }
  
  for(i=0;i<MENU_ITEMS;i++){
    char title[40]="                                    ";
    char value[40]="                                    ";
    int tidx=0;
    int vidx=0;

    // isolate title
    while(menu_strings[i][tidx+4]!='|'){
      title[tidx]=menu_strings[i][tidx+4];
      tidx++;
      if(tidx>=20) break;
    }
    title[tidx]='\0';
    
    // isolate value
    tidx++; // skip the |
    while(menu_strings[i][tidx+4+vidx]!='\0'){
      value[vidx]=menu_strings[i][tidx+4+vidx];
      vidx++;
      if(vidx>20) break;
    }
    value[vidx]='\0';
    
    if(menu_strings[i][0]=='0'){
      // first line
      u8g.setDefaultForegroundColor();
      u8g.drawBox(0, i*h+i, w, h+1);
      u8g.setDefaultBackgroundColor();
      u8g.drawStr((w-u8g.getStrWidth(title))/2, i*h+i, title);
    }
    else{
      bool s=false;
      if(menu_strings[i][1]=='*'){
        // selection
        u8g.setColorIndex(2);
        u8g.drawBox(10, i*h+i, 115, h+1);
        u8g.setDefaultBackgroundColor();
        s=true;

        if(menu_strings[i][2]=='1' && menu_strings[i][3]=='1'){
          can_edit_value=true;
        }
        else{
          can_edit_value=false;
        }        
      }
      else{
        u8g.setColorIndex(1);
      }
      
      //u8g.drawStr(0, i*h+i, menu_strings[i]);      
      u8g.drawStr(15, i*h+i, title);
      if(s){
        if(edit_value){
          u8g.setColorIndex(3);
          u8g.drawBox(140, i*h+i, 95, h+1);
          u8g.setDefaultBackgroundColor();
        }
        else{
          u8g.setDefaultForegroundColor();
        }
      }
      u8g.drawStr(150, i*h+i, value);      
    }
  }
}


uint16_t read_keys(){
  byte bitVal, ovf, mask;
  uint16_t keys=0;

  digitalWrite(KEY_LOAD, LOW);
  delayMicroseconds(KEY_PULSE_WIDTH_USEC);
  digitalWrite(KEY_LOAD, HIGH);

  for(int i = 0; i < (NUM_KEYS/8+1)*8; i++){
    bitVal = digitalRead(KEY_DATA);

    if(bitVal==0){
      // subtract with limit
      ui_keys_dbstate[i]--;
      ui_keys_dbstate[i] &= (~ui_keys_dbstate[i]) >> 7;
    }else{
      // addition with saturation
      ui_keys_dbstate[i]++;
      ovf = ui_keys_dbstate[i] & INTEGRATE_MAX;
      mask = ovf - (ovf>>INTEGRATE_SHIFT);
      ui_keys_dbstate[i]=(ui_keys_dbstate[i]^ovf)|mask;
    }

    if(i>(NUM_KEYS/8+1)*8-NUM_KEYS){ // skip keys we don't use
      // reverse bit order
      if(ui_keys_dbstate[i]==0) bitClear(keys, (NUM_KEYS/8+1)*8-i-1);
      if(ui_keys_dbstate[i]>=INTEGRATE_MAX-1) bitSet(keys, (NUM_KEYS/8+1)*8-i-1);
    }

    // next clock cycle
    digitalWrite(UI_CLOCK, HIGH);
    delayMicroseconds(KEY_PULSE_WIDTH_USEC);
    digitalWrite(UI_CLOCK, LOW);
  }

  return keys;
}


void handle_keyboard(){
  int k = read_keys();
  if(k!=key_state){
    for(int i=0;i<13;i++){
      if(bitRead(k, key_idx[i])!=bitRead(key_state, key_idx[i])){
        uint8_t msg[4] = {'m', 0, 26+octave*12+i, 0x70};
        //                                on   off
        msg[1] = !bitRead(k, key_idx[i])?0x90:0x80 | channel ;
        Serial.write(msg, 4);
      }
    }
    key_state=k;
  }
}


void setup() {
  // rotate screen, if required
  // u8g.setRot180();
  
  pinMode(uiKeySelect, INPUT_PULLUP);
  enc = encoder.read();
  setupMenu();
  menu_redraw_required = 1;     // force initial redraw

  // "keyboard"
  pinMode(UI_CLOCK, OUTPUT);
  pinMode(KEY_LOAD, OUTPUT);
  pinMode(KEY_DATA, INPUT);

  digitalWrite(UI_CLOCK, LOW);
  digitalWrite(KEY_LOAD, HIGH);

  key_state = read_keys();
  
  Serial.begin(115200);
  Serial.setTimeout(100);
}


#define BUFF_SIZE 400
char buff[BUFF_SIZE]="";
int bidx=0;
long last_data=0;
#define NO_DATA_TIMEOUT 5000


bool redraw_first=false;
void loop() {  

#if 0
  // faster, causes tearing
  if(menu_redraw_required){
    if(!redraw_first){
      u8g.firstPage();
      draw();
      redraw_first=true;
    }
    else{
      if(u8g.nextPage()){
        draw();
      }else{
        menu_redraw_required=0;
        redraw_first=false;
      }
    }
  }
#else
  // slower
  if(menu_redraw_required){
    u8g.firstPage();
    do {
      draw();
    }while(u8g.nextPage());
    menu_redraw_required=0;
  }
#endif  

  const long tm=millis();
  
  // go to idle mode if no data received
  if(disp_init && ((tm-last_data)>NO_DATA_TIMEOUT)){
    disp_init=false;
    menu_redraw_required=1;
  }

/*
  // screensaver
  if(!idle_mode && ((tm-ui_touched_tm)>IDLE_TIMEOUT)){
    idle_mode=true;
    menu_redraw_required=1;
  }
*/  

  // parse serial stream
  while(Serial.available()){
    buff[bidx] = Serial.read(); // FIXME: readBytes
    if(buff[bidx]=='}'){
      while(Serial.available()){
        Serial.read();  // clear the buffer
      }

      if(buff[0]=='{'){
        // OK, parse
        int l=0;
        int bi=1;
        char *mc = menu_strings[l];

        while(bi<bidx){
          if(buff[bi]=='\n'){
            // new line
            *mc='\0';
            mc=menu_strings[++l];
          }
          else{
            *mc=buff[bi];
            mc++;
          }
          bi++;
        }
        while(l<MENU_ITEMS){
          strcpy(menu_strings[l], "                    ");
          l++;
        }
        menu_redraw_required=1;
        if(!disp_init && tm>3000){
          disp_init=true;
        }
        last_data = tm;
      }

      bidx=0;
      break;
    }
    bidx++;
    if(bidx>=BUFF_SIZE){
      bidx=0;
    }
  }

  read_ui();
  handle_keyboard();
}


